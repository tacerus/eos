const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('templates/sign-in/sign-in-dark-theme')
})

module.exports = router
