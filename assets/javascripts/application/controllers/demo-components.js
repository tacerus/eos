$(function () {
  /* Check page classname to decide if we pull or not the data. */
  if ($('.js-demo-components').length) {
    demoComponentsShowcase()
  }
})

/**
 * Will fetch the API and return all the components-showcase files in a sigle, big array.
 */
const demoComponentsShowcase = () => {
  fetch('/api/utils/showcase')
    .then((response) => response.json())
    .then((data) => {
      console.log(data)

      data.map((ele) => {
        processComponents(ele)
      })

      $('[data-toggle="tooltip"]').tooltip()

      return $('.js-demo-components-loading').fadeOut()
    })
}

/**
 * Will map through every component and will operate over each state of the component
 * @param {*} states any component
 */
const processComponents = (states) => {
  return states.states.map((componentState) => {
    return appendComponentToPage(
      componentState,
      states.component ? states.component : 'notDefined'
    )
  })
}

/**
 * Takes a component data and component name and append it to the .js-demo-components element
 * @param {*} component component data
 * @param {*} componentName component name
 */
const appendComponentToPage = (component, componentName) => {
  const target = $('.js-demo-components')

  let _htmlStructure = ''

  component.code.map((ele) => {
    _htmlStructure += ele
  })

  const componentStatusFormated = component.status
    .toLocaleLowerCase()
    .replace(',', '')
    .replace(')', '')
    .replace('(', '')
    .replace('-', '')
    .split(' ')
    .join('-')

  return target.append(`<div style="padding: 50px; display: flex; flex-direction: column;">
    <span style="font-size: 12px;
    margin-bottom: 20px;
    background: #000000;
    padding: 5px;
    font-weight: bold;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: #fff;">${componentName} / ${componentStatusFormated}
    <span style="background: #424242; padding: 2px 5px; border-radius: 2px;">
      data-component-name=${componentName}, data-component-status=${componentStatusFormated}
    </span>
    </span>

    <section class=axe-${componentName} data-component-name=${componentName} data-component-status=${componentStatusFormated}>
     ${_htmlStructure}
    </section>
  </div>`)
}
