/* Tooltip initializer for .js-tooltip
   ========================================================================== */
$(function () {
  $('.js-tooltip').mouseover(function () {
    $(this).tooltip('show')
    $(this).mouseout(function () {
      $(this).tooltip('hide')
    })
  })
})

/* Determine when an item should show and hide a tooltip. */
$('[data-toggle="tooltip"]')
  .attr('tabindex', 0)
  .tooltip({ trigger: 'manual' })
  .focus((event) => {
    $(event.target).tooltip('show')
  })
  .blur((event) => {
    $(event.target).tooltip('hide')
  })

/* Listen for the 'escape key' so tooltips can easily be hidden */

$('body').on('keydown', function (event) {
  if (event.key === 'Escape' || 'Esc') {
    $('[data-toggle="tooltip"]').tooltip('hide')
  }
})
