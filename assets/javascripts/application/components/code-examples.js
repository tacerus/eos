/* ==========================================================================
  Code examples
  ========================================================================== */

const codeExamplesRenderer = () => {
  $('.js-code-template').each(function (i, obj) {
    const codeTemplate = $(this).html()
    $(this).siblings('.js-code-render').text(codeTemplate)
    Prism.highlightAll() // eslint-disable-line no-undef
  })
}

$(document).ready(() => {
  codeExamplesRenderer()
})
