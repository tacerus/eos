// We need to server-side render the status of the dark theme, if done in the FR it would flash.
const darkThemeMiddleware = async (req, res, next) => {
  try {
    const { cookies } = req

    if (!cookies.darkTheme) {
      // If cookie not found, create if for the first time as disabled
      res.cookie('darkTheme', 'false')
    } else if (cookies.darkTheme === 'true') {
      res.locals.isDarkTheme = true
    } else {
      res.locals.isDarkTheme = false
    }

    return next()
  } catch (error) {
    console.log('error: ', error)
    return next()
  }
}

module.exports = {
  darkThemeMiddleware
}
