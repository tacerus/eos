const axios = require('axios')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')

const readFileAsync = promisify(fs.readFile)

// IMPORTANT: this script can be further improved. It was created with the intention for a first step and not having to generate all the files by hand and set a base for the future
module.exports.autoGenerateA11TestFiles = async () => {
  const template = path.join(
    `${process.cwd()}/modules/templates/a11y-test.template.js`
  )

  const componentsList = await axios.get(
    'http://localhost:3000/api/utils/showcase'
  )
  const ayy1TestTemplate = await readFileAsync(template, { encoding: 'utf8' })

  componentsList.data.forEach((element) => {
    // Node does not support replaceAll, so we need to do it for every match.
    const replaceTemplateFiles = ayy1TestTemplate
      .replace('#{componentName}', element.component)
      .replace('#{componentName}', element.component)
      .replace('#{componentName}', element.component)

    const writePath = path.join(
      `${process.cwd()}/cypress/integration/accessibility/${
        element.component
      }.spec.js`
    )

    if (!fs.existsSync(writePath)) {
      return fs.writeFile(writePath, replaceTemplateFiles, (err, data) => {
        if (err) throw err
      })
    }
  })

  console.log(`✅ Done generating new files`)
}

this.autoGenerateA11TestFiles()
