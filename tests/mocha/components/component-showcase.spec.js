// these variables need to be defined in the global scope
// so renderComponentShowcase can access them instead of the ones defined in
// the controller

const getComponentService = sinon.fake()

describe('Component showcase', () => {
  describe('should have different states for elements ', () => {
    const stateWrap = $(
      '<ul class="js-showcase-list"><li class="js-showcase-current"><input class="js-element-id" type="radio" name="text"><label class="js-element-label"></label></li></ul><p class="js-how-to-use"></p><div class="js-example-inner-box"></div><pre class="js-snippet-code"><code></code></pre>'
    )
    const collection = [
      {
        component: 'text',
        states: [
          {
            status: 'default',
            how_to_use: 'default state',
            code: ['<label>default</label>']
          },
          {
            status: 'focused',
            how_to_use: 'focused state',
            code: ['<label>focused</label>']
          },
          {
            status: 'disabled',
            how_to_use: 'disabled state.',
            code: ['<label>disabled</label>']
          }
        ]
      }
    ]

    before((done) => {
      $('body').prepend(stateWrap)
      done()
    })

    after((done) => {
      $(stateWrap).remove()
      done()
    })

    it('Should print the states of element for input type', () => {
      $eleDisplayTemplate = $('.js-showcase-current').clone(true)
      const component = $('.js-element-id').attr('name')

      renderComponentShowcase(collection, component)

      expect($('.js-showcase-current')).to.have.lengthOf(4)
      expect($('.js-element-label')).to.contain('Default')
      expect($('.js-element-label')).to.contain('Focused')
      expect($('.js-element-label')).to.contain('Disabled')
    })

    it('Should print the how to use and code snippet text of selected state ', () => {
      const componentCollection = collection
        .filter((ele) => ele.component === 'text')
        .map((ele) => ele.states)

      updateComponentShowcase(componentCollection[0], 'default')

      expect($('.js-how-to-use')).to.contain('default state')
      expect($('.js-snippet-code code')).to.contain('<label>default</label>')
    })
  })
})
