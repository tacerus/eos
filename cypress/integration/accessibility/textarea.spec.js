describe.skip('textarea a11y testing', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.injectAxe()
    cy.wait(600)
  })

  context(
    'Testing all statuses at once for common, shared funtionality',
    () => {
      // Applying a context and run parameters
      it('should comply with wcag21aa', () => {
        cy.onlyOn('development')
        cy.checkA11y('[data-component-name="textarea"]', {
          run: 'wcag21aa'
        })
      })
    }
  )

  context(
    'Testing disabled statuses at once for common, shared funtionality',
    () => {
      it('should have disable propriety and aria-hidden attributes', () => {
        cy.get(
          '[data-component-name="textarea"][data-component-status="disabled"] label'
        )
          .invoke('attr', 'aria-label')
          .should('eql', 'Textarea is disabled')

        cy.get(
          '[data-component-name="textarea"][data-component-status="disabled"] textarea'
        ).each(($item) => {
          cy.wrap($item).invoke('prop', 'disabled').should('eql', true)
        })
      })
    }
  )
})
