describe('Check for specific part that repeats across multiple componentes', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.wait(600)
  })

  context('Inputs validation', () => {
    // Applying a context and run parameters
    it('should check that every error field is vissible and it has the correct icon', () => {
      // cy.get('.error-msg .eos-icons-outlined').contains("error")
      cy.get('.error-msg .eos-icons-outlined')
        .should('be.visible')
        .each(($item) => {
          cy.wrap($item).should('contain', 'error') // checks all children of <td>
        })
    })
  })
})
