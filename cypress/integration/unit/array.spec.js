/// <reference types="cypress" />
const {
  compareSameArrays,
  uniqueArrayFromArrays,
  elementIncludedInArray
} = require('../../../modules/utils/array.utils')

describe('#array.utils.js', () => {
  context('compareSameArrays', () => {
    it('should fail, with no inputs', () => {
      expect(compareSameArrays).to.throw()
    })

    it('should fail, any of the arrays are empty', () => {
      expect(compareSameArrays([], []).status).to.eql('error')
    })

    it('should fail, when the length does not match', () => {
      expect(
        compareSameArrays(['admin', 'edit'], ['admin', 'edit', 'extra-item'])
      ).to.be.false
      // expect(compareSameArrays(['admin'], ['admin']).status).to.eql('error')
    })

    it('should fail, arrays dont match', () => {
      expect(compareSameArrays([1, 2, 3], [1, 2])).to.be.false
      expect(compareSameArrays([1, 2, 3], [1, 2, 'hello'])).to.be.false
      expect(compareSameArrays([1], ['hello'])).to.be.false
    })

    it('should pass, arrays  match', () => {
      expect(compareSameArrays([1, 2, 3], [1, 2, 3])).to.be.true
      expect(compareSameArrays([1, 2, 'hello'], [1, 2, 'hello'])).to.be.true
    })
  })

  context('uniqueArrayFromArrays', () => {
    it('should fail, with no inputs', () => {
      expect(uniqueArrayFromArrays).to.throw()
    })

    it('should pass, with single correct input', () => {
      expect(uniqueArrayFromArrays([1, 2, 3, 1, 2, 3, 4])).to.eql([1, 2, 3, 4])
      expect(
        uniqueArrayFromArrays([
          'close',
          'close',
          'info',
          'warning',
          'error',
          'edit',
          'warning'
        ])
      ).to.eql(['close', 'info', 'warning', 'error', 'edit'])
    })

    it('should pass, with two correct input', () => {
      expect(uniqueArrayFromArrays([1, 2, 3], [1, 2, 3, 4])).to.eql([
        1,
        2,
        3,
        4
      ])
      expect(
        uniqueArrayFromArrays(['admin', 'left_arrow'], ['admin', 'left_arrow'])
      ).to.eql(['admin', 'left_arrow'])
      expect(
        uniqueArrayFromArrays(['admin', 'left_arrow'], ['admin', 'right_arrow'])
      ).to.eql(['admin', 'left_arrow', 'right_arrow'])
    })
  })

  context('elementIncludedInArray', () => {
    it('should throw an error if paramentes are missing or the array empty', () => {
      expect(elementIncludedInArray).to.throw('Key or Array was not provided')
      expect(() => elementIncludedInArray('key', [])).to.throw(
        'Key or Array was not provided'
      )
    })

    it('should throw an error if an element of the array is an empty string', () => {
      expect(() =>
        elementIncludedInArray('Breaking: This is a test', ['New:', 'Fix:', ''])
      ).to.throw()
      expect(() =>
        elementIncludedInArray('New: This is a test', ['New:', 'Fix:', ''])
      ).to.throw()
    })

    it('should return an empty array if no matching element is found', () => {
      expect(
        elementIncludedInArray('Breaking: This is a test', ['New:', 'Fix:'])
      ).length(0)
      expect(
        elementIncludedInArray('New: This is a test', ['Breaking:', 'Fix:'])
      ).length(0)
    })

    it('should return the matching element and length 1', () => {
      expect(elementIncludedInArray('ux', ['ux', 'a11y', 'ops'])).to.eql(['ux'])
    })

    it('should return the matching element and length 1 even when one element its in uppercase', () => {
      expect(elementIncludedInArray('UX', ['ux', 'a11y', 'ops'])).to.eql(['ux'])
    })

    it('should only match the ux element and ignore the ui from the string', () => {
      expect(
        elementIncludedInArray('ux/ui-changes', ['ux', 'a11y', 'ops', 'ui'])
      ).to.eql(['ux'])
    })

    it('should match the New prefix from the string', () => {
      expect(
        elementIncludedInArray('New: This is a test', [
          'New:',
          'Fix:',
          'Breaking:'
        ])
      ).to.eql(['New:'])
      expect(
        elementIncludedInArray('Fix: This is a test', [
          'New:',
          'Fix:',
          'Breaking:'
        ])
      ).to.eql(['Fix:'])
      expect(
        elementIncludedInArray('Breaking: This is a test', [
          'New:',
          'Fix:',
          'Breaking:'
        ])
      ).to.eql(['Breaking:'])
    })

    it('should only match the first result', () => {
      expect(
        elementIncludedInArray('New: new test', ['New:', 'Fix:', 'Breaking:'])
      ).to.eql(['New:'])
    })
    it('should not match when commit message its without the : ', () => {
      expect(
        elementIncludedInArray('New test description', [
          'New:',
          'Fix:',
          'Breaking:'
        ])
      ).to.eql([])
    })
  })
})
